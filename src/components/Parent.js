import { Component } from "react";
import Display from "./display";
class Parent extends Component {
    constructor (props){
        super(props);
        this.state = {
            display: false
        }
    }
    btnUpdateDisplay = () =>{
        this.setState({
            display: true
        })
    }
    render(){
        return(
            <div className="text-center mt-5 container bg-dark" style={{padding:"50px", width:"300px"}}>
                <div>
                    {this.state.display?<button className="btn btn-danger">Destroy Component</button>:<button className="btn btn-info text-white" onClick={this.btnUpdateDisplay}>Create Component</button>}
                </div>
                <div>
                    {this.state.display?<Display/>:null}
                </div>
                
            </div>
        )
    }
}
export default Parent;